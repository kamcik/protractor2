import { browser, element, by } from 'protractor'
import CommonPage from '../objects/commonPage'
import * as common from '../lib/common'

describe('Calculations', function () {
    beforeAll(() => {
        console.log('Starting suite execution.');
    });
    afterAll(() => {
        console.log('Suite execution reached the end.');
    });
    beforeEach(() => {
        console.log('Starting test execution.');
        browser.get('http://juliemr.github.io/protractor-demo/');
    });
    afterEach(() => {
        console.log('Test execution reached the end.');
    });

    it('Sum one and two', function () {
        common.sumAndCheck(1, 2)
        expect(CommonPage.history.count()).toEqual(1);
    });

    it('Sum four and six', function () {
        common.sumAndCheck(4, 6)
        expect(CommonPage.history.count()).toEqual(1);
    });

    it('All calculations has a record in history', function () {
        common.sumAndCheck(1, 1);
        common.sumAndCheck(2, 2);
        expect(CommonPage.history.count()).toEqual(2);
        common.sumAndCheck(6, 6);
        expect(CommonPage.history.count()).toEqual(3);
        expect(CommonPage.history.last().getText()).toContain('1 + 1');
        expect(CommonPage.history.first().getText()).toContain(12);
    });
});