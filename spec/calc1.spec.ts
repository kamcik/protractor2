import { browser, by, element } from 'protractor'

describe('Calculator page availability', () => {
    beforeAll(() => {
        console.log('Starting suite execution.');
    });
    afterAll(() => {
        console.log('Suite execution reached the end.');
    });
    beforeEach(() => {
        console.log('Starting test execution.');
        browser.get('http://juliemr.github.io/protractor-demo/');
    });
    afterEach(() => {
        console.log('Test execution reached the end.');
    });

    it('Calculator page is visible', () => {
        expect(browser.getTitle()).toEqual('Super Calculator');
        browser.sleep(1000)
    });

});