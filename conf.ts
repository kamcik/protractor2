import * as fs from 'fs'
import { browser, Config } from 'protractor';
import { JUnitXmlReporter, TerminalReporter } from 'jasmine-reporters'
import * as HTMLReport from 'protractor-html-reporter'

export let config: Config = {
    // All conf options are available in https://github.com/angular/protractor/blob/master/lib/config.ts
    baseUrl: 'http://juliemr.github.io/protractor-demo/',

    framework: 'jasmine2',

    specs: ['spec/*.spec.js'],

    suites: {
        // run by command "protractor conf.js --suite <suite name>,<suite name>". You can use also 
        calc1: 'spec/calc1.spec.js',
        calc2: 'spec/calc2.spec.js',
        calc: ['spec/calc1.spec.js', 'spec/calc2.spec.js',],
    },

    capabilities: {
        // List of chrome options https://sites.google.com/a/chromium.org/chromedriver/capabilities
        browserName: 'chrome',
        chromeOptions: {
            args: ['--start-maximized', '--disable-extensions', '--headless']
        },
    },

    // jasmine-reporters proividing XML output
    onPrepare: () => {
        jasmine.getEnv().addReporter(new TerminalReporter({
            consolidateAll: true,
            displayStacktrace: true
        }));

        jasmine.getEnv().addReporter(new JUnitXmlReporter({
            consolidateAll: true,
            savePath: 'testresults',
            filePrefix: 'xmloutput',
            captureStdout: true,
        }));

        // jasmine.getEnv().addReporter({
        //     specDone: (result) => {
        //         if (result.status == 'passed') {
        //             browser.takeScreenshot()
        //                 .then((png) => {
        //                     fs.writeFile(`${__dirname}/screenshots/${result.id}.png`, png, 'base64', (err) => {
        //
        //                     })
        //                 })
        //         }
        //     }
        // })
    },

    onComplete: () => {
        browser.getCapabilities().then(function (caps) {
            const testConfig = {
                reportTitle: 'Test Execution Report',
                outputPath: './',
                screenshotPath: './screenshots',
                testBrowser: caps.get('browserName'),
                browserVersion: caps.get('version'),
                modifiedSuiteName: false,
                screenshotsOnlyOnFailure: true
            };

            new HTMLReport().from('testresults/xmloutput.xml', testConfig);
        });
    },

// jasmineNodeOpts: {
//     showColors: true, // Use colors in the command line report.
//     defaultTimeoutInterval: 15000, // Default time to wait in ms before a test fails.
//     isVerbose: true,
//     includeStackTrace: true,
// },

// params: {
//     auth: {
//         user: 'protractor-br',
//         password: '#ng123#'
//     }
// },
}