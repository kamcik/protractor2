import { $, browser, by, element } from 'protractor';

class CommonPage {
    public firstNumber: any = element(by.model('first'));
    public secondNumber: any = element(by.model('second'));
    public goButton: any = element(by.id('gobutton'));
    public latestResult: any = element(by.binding('latest'));
    public currentResult: any = element(by.xpath('/html/body/div/div/form/h2'));
    public history: any = element.all(by.repeater('result in memory'));
}

export default new CommonPage();
