import { browser, by, element } from 'protractor'
import CommonPage from '../objects/commonPage'

export function sumAndCheck(a, b) {
    CommonPage.firstNumber.sendKeys(a);
    CommonPage.secondNumber.sendKeys(b);
    CommonPage.goButton.click();
    expect(CommonPage.currentResult.getText()).toEqual((a + b).toString());
    browser.driver.sleep(500);
}